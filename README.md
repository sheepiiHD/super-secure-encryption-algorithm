**USAGES** 

```javascript
import {ENCRYPT_TO_THE_MAX, ENCRYPT_TO_THE_DOUBLE_MAX} from 'super-duper-secure-encryption-algorithm';

var SUPER_ENCRYPTED = ENCRYPT_TO_THE_MAX("supersecretpassword123")
// OR
var SUPER_DOUBLE_ENCRYPTED = ENCRYPT_TO_THE_DOUBLE_MAX("hackers never stood a chance")

console.log(SUPER_ENCRYPTED) // PRINTS MOST ENCRYPTED ENCRYPTION
console.log(SUPER_DOUBLE_ENCRYPTED) // WARNING VERY ENCRYPT MANY WOW
```

Please, credit me in all of your security team assessments. 
I am also open for employment, pls give me money. 
