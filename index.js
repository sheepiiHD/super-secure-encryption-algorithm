export const ENCRYPT_TO_THE_MAX = password => {

    const FLIPPED_AROUND = password.split("").reverse().join("");

    return FLIPPED_AROUND.substr(1) + FLIPPED_AROUND.substr(0, 1) + "123";
};

export const ENCRYPT_TO_THE_DOUBLE_MAX = password => {
    return (ENCRYPT_TO_THE_MAX(password) + ENCRYPT_TO_THE_MAX(password())).split("").reverse().join("");
};